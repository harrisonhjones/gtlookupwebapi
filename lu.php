<?php
// Disable error reporting
error_reporting(0);

// Error variables
$method_error = array("error" => $method . " is not supported. This api can only be used via GET");
$connection_error = array("error" => "Unable to connect to the directory server. Please contact harrisonhjones@gmail.com if this persists");	
$invalid_parameters = array("error" => "Invalid parameters. Usage: lu.php/term/filter/format");

// Grab the request method (Put, Get, Post, etc)	
$method = $_SERVER['REQUEST_METHOD'];
$request = explode("/", substr(@$_SERVER['PATH_INFO'], 1));

switch ($method) {
	case 'PUT':
		echo json_encode($method_error);
		break;
	case 'POST':
		echo json_encode($method_error);
		break;
	case 'GET':
		if (isset($_GET['about']))
		{
			echo json_encode($about);
		}
		else
		{
			$query = $_GET['query'];
			if ($query == "")
			{
				echo json_encode($invalid_parameters);
			}
			else
			{
				$url = "lu-serve.gatech.edu";
				$port = 105;

				$directory_arr = array(array());

				$fp = fsockopen($url, $port, $errno, $errstr, 1);
				$pattern = "/^200:Ok/";

				if (!$fp) {
					echo json_encode($connection_error);
				} else {
					$out = $query . " return all\n";
					fwrite($fp, $out);
					$data = "";
					while (!feof($fp)) {
						$incoming = fgets($fp, 128);
						$data .= $incoming;
						if (preg_match($pattern, $incoming )){fwrite($fp, "quit\n");}
					}
					$data =str_replace ("-200:", "", $data);
					$exploded_data = explode("\n",$data);
					foreach ($exploded_data as $line)
					{
						$data = explode(":",$line);
						if ($data[1] == "Ok.")
							break;
						$field = str_replace(" ","",$data[1]);
						$directory_arr[$data[0]-1][$field] = substr($data[2],1);
					}
					foreach ($directory_arr as $result)
					{
						if ($result['type'] == "student")
						{$students[] = $result;}
						else
						{$employees[] = $result;}
					}
					$type = $_GET['type'];
					if ($type == "student")
					{echo json_encode($students);}
					elseif ($type == "employee")
					{echo json_encode($employees);}
					else
					{echo json_encode($directory_arr);}
				
					fclose($fp);
				}
			}
		}
		break;
	case 'HEAD':
		echo json_encode($method_error);
		break;
	case 'DELETE':
		echo json_encode($method_error);
		break;
	case 'OPTIONS':
		echo json_encode($method_error);
		break;
	default:
		echo json_encode($method_error);
		break;
}

?>