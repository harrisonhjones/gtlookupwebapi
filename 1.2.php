<?php
// Disable error reporting
error_reporting(0);

// Error variables
$method_error = array("error" => $method . " is not supported. This api can only be used via GET");
$connection_error = array("error" => "Unable to connect to the directory server. Please contact harrisonhjones@gmail.com if this persists");	
$invalid_parameters = array("error" => "Invalid parameters. Usage: lu.php/term/filter");

// Grab the request method (Put, Get, Post, etc)	
$method = $_SERVER['REQUEST_METHOD'];
$request = explode("/", substr(@$_SERVER['PATH_INFO'], 1));

switch ($method) {
	case 'PUT':
		echo json_encode($method_error);
		break;
	case 'POST':
		echo json_encode($method_error);
		break;
	case 'GET':
		$uri = urldecode($_SERVER['REQUEST_URI']);
		$baseURL = "api/lu/1.2.php";

		$uri = trim(str_replace($baseURL, "", $uri),"/");
		$pc = explode("/", $uri);
		$term = $pc[0];

		$filter = $pc[1];

		if ($term == "")
		{
			echo json_encode($invalid_parameters);
		}
		else
		{
			$url = "lu-serve.gatech.edu";
			$port = 105;

			$directory_arr = array(array());

			$fp = fsockopen($url, $port, $errno, $errstr, 1);
			$pattern = "/^200:Ok/";

			if (!$fp) {
				echo json_encode($connection_error);
			} else {
				$out = $term . " return all\n";
				fwrite($fp, $out);
				$data = "";
				while (!feof($fp)) {
					$incoming = fgets($fp, 128);
					$data .= $incoming;
					if (preg_match($pattern, $incoming )){fwrite($fp, "quit\n");}
				}
				$data =str_replace ("-200:", "", $data);
				$exploded_data = explode("\n",$data);
				foreach ($exploded_data as $line)
				{
					$data = explode(":",$line);
					if ($data[1] == "Ok.")
						break;
					$field = str_replace(" ","",$data[1]);
					$directory_arr[$data[0]-1][$field] = substr($data[2],1);
				}
				foreach ($directory_arr as $result)
				{
					if ($result['type'] == "student")
					{$students[] = $result;}
					else
					{$employees[] = $result;}
				}
				if ($filter == "student")
				{$array = array("results" => $students);}
				elseif ($filter == "employee")
				{$array = array("results" => $employees);}
				else
				{$array = array("results" => $directory_arr);}
				echo json_encode($array);
				fclose($fp);
			}
		}
		break;
	case 'HEAD':
		echo json_encode($method_error);
		break;
	case 'DELETE':
		echo json_encode($method_error);
		break;
	case 'OPTIONS':
		echo json_encode($method_error);
		break;
	default:
		echo json_encode($method_error);
		break;
}

?>