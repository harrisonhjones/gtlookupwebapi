# README:

## GTLookup Web API
Simple RESTful (GET only) web API for the Georgia Tech Directory.

## Author:
Harrison Jones (harrison@hhj.me)

## Usage:
/lu/term/filter

### term:
The name, email, id of the user you are searching for
	
### filter:
Filter by "employee" or "student". Only show "employee" or "student" results

##Results:
Output is in the form of a JSON string	